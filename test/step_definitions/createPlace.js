const {I} = inject();
// Add in your custom step files

When('я нахожусь на странице Вход', () => {
    I.amOnPage('login')
});

When('я ввожу {string} в поле {string}', (text, fieldName) => {
    I.fillField({xpath: `//input[@id='${fieldName}']`}, text)
});

When('я нажимаю на кнопку {string}', (buttonName) => {
    I.click(`//button[.='${buttonName}']`)
});

When('я нахожусь на странице добавить заведение', () => {
    I.amOnPage('add');
});

When('я ввожу {string} в поле {string}', (text, fieldName) => {
    I.fillField({xpath: `//input[@id='${fieldName}']`}, text)
});

When('я ввожу описание заведения {string} в поле {string}', (text, fieldName) => {
    I.fillField({xpath: `//input[@id='${fieldName}']`}, text)
});

When('я загружаю картинку', () => {
    I.attachFile({xpath: "//input[@id='image']"}, 'assets/bar.jpg');
});

When('я соглашаюсь с условиями', () => {
    I.click({xpath: "//input[@id='1']"})
});

When('я нажимаю на кнопку {string}', (buttonName) => {
    I.click(`//button[.='${buttonName}']`)
});

When('я нахожусь на главной странице', () => {
    I.amOnPage('')
});

When('я вижу текст {string}', (text) => {
    I.see(text);
});

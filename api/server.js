const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const app = express();

const users = require('./routes/users');
const institutions = require('./routes/institutions');
const review = require('./routes/review');
const image = require('./routes/images');

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {

    app.use('/users', users);
    app.use('/institutions', institutions);
    app.use('/review', review);
    app.use('/images', image);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});

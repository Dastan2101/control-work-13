const nanoid = require('nanoid');

const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');
const Institution = require('./models/Institution');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const user = await User.create({
            username: "John",
            password: "123",
            role: "user",
            token: nanoid()
        },
        {
            username: "Sem",
            password: "123",
            role: "user",
            token: nanoid()
        },
        {
            username: 'admin',
            password: '123',
            role: 'admin',
            token: nanoid()
        });

    await Institution.create({
            user: user[0],
            title: 'DANDELYAN, LONDON',
            description: 'If being considered the best requires meeting the highest of standards, it also requires surpassing them. The innovative Dandelyan, winner of The World’s Best Bar Award, sponsored by Perrier, has done that in...',
            image: 'sbb.jpg',
            isAgree: true,
        },
        {
            user: user[1],
            title: 'MANHATTAN, SINGAPORE',
            description: 'As entrances go, the walk up to the back bar at Manhattan is about as regal as it gets. It oozes chic from every fold in its glorious soft furnishings and Chesterfield armchairs, inviting you to settle in and...',
            image: 'bar2.jpg',
            isAgree: true,
        },
        {
            user: user[1],
            title: 'THE NOMAD, NEW YORK',
            description: 'The NoMad bar, set in the mid-town New York hotel of the same name, is in The World’s 50 Best Bars top 10 for the third year straight. What sets the NoMad aside from other hotel bars is...',
            image: 'bastion.png',
            isAgree: true,
        }
    );


    return connection.close();
};


run().catch(error => {
    console.error('Something wrong happened...', error);
});

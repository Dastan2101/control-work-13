const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const InstitutionSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    images: {
        type: Schema.Types.ObjectId,
        ref: 'Image',
    }
});

const Institution = mongoose.model('Institution', InstitutionSchema);

module.exports = Institution;
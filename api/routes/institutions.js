const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const Institution = require('../models/Institution')

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    Institution.find().populate('images').then(result => {
        if (result) {
            res.send(result)
        } else return res.sendStatus(404)
    }).catch(() => res.sendStatus(500))

});

router.get('/:id', async (req, res) => {
    Institution.findById(req.params.id).populate('user')
        .then(result => {
            if (result) {
                res.send(result)
            } else return res.sendStatus(404)
        }).catch(() => res.sendStatus(500))
});

router.post('/', auth, upload.single('image'), (req, res) => {

    const reqData = req.body;

    if (reqData.isAgree) {
        reqData.image = req.file.filename;

        const institution = new Institution({
            user: req.user._id,
            title: reqData.title,
            description: reqData.description,
            image: reqData.image,
        });

        institution.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    }
});

router.delete('/:id', [auth, permit('admin')], (req, res) => {
    Institution.findById(req.params.id)
        .then(result => {
            if (result) {
                result.remove()
                res.send({message: 'Success removed'})
            } else return res.sendStatus(404)
        }).catch(() => res.sendStatus(500))
});



module.exports = router;

const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const Image = require('../models/Image')
const Institution = require('../models/Institution')

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();


router.post('/', auth, upload.single('image'), (req, res) => {

    const data = req.body;

    data.image = req.file.filename;

    const image = new Image({
        user: req.user._id,
        institution: req.query.institution,
        image: data.image,
    });

    image.save()
        .then(result => {
            res.send(result)})
        .catch(error => res.status(400).send(error));
});


router.get('/:id', (req, res) => {
    Image.find({institution: req.params.id})
        .then(result => {
            if (result) {
                res.send(result)
            } else return res.sendStatus(404)
        }).catch(() => res.sendStatus(500))
});

router.delete('/:id', [auth, permit('admin')], (req, res) => {
    Image.findById(req.params.id)
        .then(result => {
            if (result) {
                result.remove()
                res.send({message: 'Success removed'})
            } else return res.sendStatus(404)
        }).catch(() => res.sendStatus(500))
});


module.exports = router;

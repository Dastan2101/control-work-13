const express = require('express');

const Review = require('../models/Review')

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');


const router = express.Router();


router.get('/:id', (req, res) => {
    Review.find({institution: req.params.id}).populate('user').sort({date: -1})
        .then(result => {
            if (result) {
                res.send(result)
            } else return res.sendStatus(404)
        }).catch(() => res.sendStatus(500))
});

router.post('/', auth, (req, res) => {

    const data = req.body

    const review = new Review({
        user: req.user._id,
        institution: req.query.institution,
        review: data.review,
        date: new Date(),
        quality: data.quality,
        service: data.service,
        interior: data.interior
    });
    review.save()
        .then(result => res.send({message: 'Success'}))
        .catch(error => res.status(400).send(error));
});


router.delete('/:id', [auth, permit('admin')], (req, res) => {
    Review.findById(req.params.id)
        .then(result => {
            if (result) {
                result.remove()
                res.send({message: 'Success removed'})
            } else return res.sendStatus(404)
        }).catch(() => res.sendStatus(500))
});



module.exports = router;

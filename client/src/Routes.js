import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Register from "./container/Register/Register";
import Login from "./container/Login/Login";
import MainPage from "./container/MainPage/MainPage";
import FullInformation from "./container/FullInformation/FullInformation";
import NewPlaceForm from "./container/NewPlaceForm/NewPlaceForm";


const ProtectedRoute = ({isAllowed, ...props}) => {
    return isAllowed ? <Route {...props} /> : <Redirect to="/login"/>
};

const Routes = ({user}) => {
    return (
        <Switch>
            <Route path="/" exact component={MainPage}/>
            <ProtectedRoute
                isAllowed={user}
                path="/add"
                exact
                component={NewPlaceForm}
            />
            <Route path="/information/:id" exact component={FullInformation}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
        </Switch>
    );
};

export default Routes;
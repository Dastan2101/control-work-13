import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {connect} from "react-redux";
import {createPlace} from "../../store/actions/institutionsActions";

class NewPlaceForm extends Component {

    state = {
        title: '',
        description: '',
        image: '',
        isAgree: false
    };

    submitFormHandler = event => {
        event.preventDefault();
        if (this.state.isAgree) {

            const formData = new FormData();

            Object.keys(this.state).forEach(key => {
                formData.append(key, this.state[key]);
            });

            this.props.createPlace(formData);
        }

    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    checkBoxHandler = (event) => {
        if (event.target.checked) {
            this.setState({isAgree: true})
        }
    };

    render() {
        return (
            <React.Fragment>
                <h1>Add new place</h1>
                <Form onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="title"
                        title="Title"
                        required
                        type="text"
                        onChange={this.inputChangeHandler}
                        value={this.state.title}
                        id={'title'}
                    />
                    <FormElement
                        propertyName="description"
                        title="Description"
                        type="text"
                        required
                        onChange={this.inputChangeHandler}
                        value={this.state.description}
                        id={'description'}
                    />

                    <FormElement
                        propertyName="image"
                        title="Main image"
                        type="file"
                        required
                        onChange={this.fileChangeHandler}
                        id={'image'}
                    />
                    <FormGroup row>
                        <Label sm={2} for={'isAgree'}>
                            This filed a required
                        </Label>
                        <Col sm={10}>
                            <Input
                                name={'isAgree'}
                                id={'1'}
                                type="checkbox"
                                onChange={this.checkBoxHandler}
                                required
                                className={'checkbox'}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary" className={'Submit'}>Submit</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createPlace: (data) => dispatch(createPlace(data))
});


export default connect(null, mapDispatchToProps)(NewPlaceForm);

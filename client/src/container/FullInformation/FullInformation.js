import React, {Component} from 'react';
import {fetchFullInfo} from "../../store/actions/institutionsActions";
import {connect} from "react-redux";


import {apiURL} from "../../constants";
import StarRatings from "react-star-ratings";
import {createReviewSuccess, fetchReviews, removeReview} from "../../store/actions/reviewsActions";
import {Button} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {fetchImages, removeImage, uploadsImage} from "../../store/actions/imagesActions";

import './FullInformation.css'

class FullInformation extends Component {

    state = {
        review: '',
        quality: 0,
        service: 0,
        interior: 0,
        image: '',
        error: false
    };

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.fetchFullInfo(id)
        this.props.fetchReviews(id)
        this.props.fetchImages(id)
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };


    uploadImage = event => {
        event.preventDefault();

        const formData = new FormData();

        formData.append('image', this.state.image)
        const id = this.props.match.params.id;
        this.props.uploadsImage(id, formData)

    };

    createReview = () => {
        const id = this.props.match.params.id;
        delete this.state.image
        if (this.state.review === '') {
            this.setState({error: true})
        } else {
            this.props.createReviewSuccess(id, this.state)
            this.setState({
                review: '',
                quality: 0,
                service: 0,
                interior: 0,
                error: false
            })
        }
    };

    removeImageById = (id) => {
        const instId = this.props.match.params.id
        this.props.removeImage(id, instId)
    };

    removeReviewById = id => {
        const instId = this.props.match.params.id
        this.props.removeReview(id, instId)
    };

    render() {
        const {information, user, reviews, images} = this.props

        const overAllQuality = reviews.length && reviews.reduce((acc, val) => acc += val.quality || 0, 0);
        const overAllService = reviews.length && reviews.reduce((acc, val) => acc += val.service || 0, 0);
        const overAllInterior = reviews.length && reviews.reduce((acc, val) => acc += val.interior || 0, 0);
        const quality = overAllQuality / reviews.length
        const service = overAllService / reviews.length
        const interior = overAllInterior / reviews.length
        const overAll = (quality + service + interior) / 3

        return (
            <div>
                {information &&
                <React.Fragment>
                    <h1>{information.title}</h1>
                    <div className={'information-main'}>
                        <h3>{information.description}</h3>
                        <img className={'main-image'} src={apiURL + '/uploads/' + information.image} alt="#"/>
                    </div>
                    <div className={'information-gallery'}>
                        <h3>Gallery:</h3>
                        {
                            images.length ? images.map(item => (
                                    <div className={'images-block'} key={item._id}>
                                        <img className={'gallery-image'} src={apiURL + '/uploads/' + item.image}
                                             alt="#"/>
                                        {user &&
                                        user.role === 'admin' &&
                                        <button className={'removeIcon'} onClick={() => this.removeImageById(item._id)}/>
                                        }
                                    </div>
                                ))
                                : <p>Empty gallery</p>
                        }
                    </div>
                </React.Fragment>
                }
                <div className='information-block'>
                    <h3>Ratings: </h3>
                    <div className={'rating-block'}>
                        <h4 className={'rating-title'}>Overall: </h4>
                        <StarRatings
                            rating={overAll || 0}
                            starRatedColor="red"
                            numberOfStars={6}
                            name='rating'
                            starDimension="17px"
                            starSpacing="2px"
                            isSelectable={false}
                        />
                        <p className={'rating-value'}>{overAll ? overAll.toFixed(2) : 'No rating yet'}</p>
                    </div>
                    <div className={'rating-block'}>
                        <h5 className={'rating-title'}>Quality of food: </h5>
                        <StarRatings
                            rating={quality || 0}
                            starRatedColor="red"
                            numberOfStars={6}
                            isAggregateRating={true}
                            name='rating'
                            starDimension="17px"
                            starSpacing="2px"
                            isSelectable={false}
                        />
                        <p className={'rating-value'}>{quality ? quality.toFixed(2) : 'No rating yet'}</p>
                    </div>
                    <div className={'rating-block'}>
                        <h5 className={'rating-title'}>Service quality: </h5>
                        <StarRatings
                            rating={service || 0}
                            starRatedColor="red"
                            numberOfStars={6}
                            name='rating'
                            starDimension="17px"
                            starSpacing="2px"
                            isSelectable={false}
                        />
                        <p className={'rating-value'}>{service ? service.toFixed(2) : 'No rating yet'}</p>
                    </div>
                    <div className={'rating-block'}>
                        <h5 className={'rating-title'}>Interior: </h5>
                        <StarRatings
                            rating={interior || 0}
                            starRatedColor="red"
                            numberOfStars={6}
                            name='rating'
                            starDimension="17px"
                            starSpacing="2px"
                            isSelectable={false}
                        />
                        <p className={'rating-value'}>{interior ? interior.toFixed(2) : 'No rating yet'}</p>
                    </div>
                </div>
                <div className='information-block'>
                    <h3>Reviews: </h3>
                    {
                        reviews.length ? reviews.map(it => (
                                <div key={it._id} className={'reviews'}>
                                    <h5>Author: <i>{it.user.username}</i></h5>
                                    <h5>Said: <i>{it.review}</i></h5>
                                    <h6>date: {new Date(it.date).toISOString().split('T')[0]}</h6>
                                    <div className={'rating-block'}>
                                        <h5 className={'rating-title'}>Quality of food: </h5>
                                        <StarRatings
                                            rating={it.quality}
                                            starRatedColor="blue"
                                            numberOfStars={6}
                                            name='rating'
                                            starDimension="15px"
                                            starSpacing="2px"
                                            isSelectable={false}
                                        />
                                    </div>
                                    <div className={'rating-block'}>
                                        <h5 className={'rating-title'}>Interior: </h5>
                                        <StarRatings
                                            rating={it.interior}
                                            starRatedColor="blue"
                                            numberOfStars={6}
                                            name='rating'
                                            starDimension="15px"
                                            starSpacing="2px"
                                            isSelectable={false}
                                        />
                                    </div>
                                    <div className={'rating-block'}>
                                        <h5 className={'rating-title'}>Service quality: </h5>
                                        <StarRatings
                                            rating={it.service}
                                            starRatedColor="blue"
                                            numberOfStars={6}
                                            name='rating'
                                            starDimension="15px"
                                            starSpacing="2px"
                                            isSelectable={false}
                                        />
                                    </div>
                                    {user &&
                                    user.role === 'admin' &&
                                    <button className={'removeIcon'} onClick={() => this.removeReviewById(it._id)}/>}
                                </div>
                            ))
                            : <h6>No reviews yet</h6>
                    }
                </div>
                {user &&
                <div className='information-block'>
                    <h3>Add review:</h3>
                    <textarea
                        name="review"
                        value={this.state.review} id="comment"
                        onChange={this.inputChangeHandler}
                        className={'text-area'}/>
                    {this.state.error && <p style={{color: 'red'}}>Required field</p>}
                    <div className={'rating-root'}>
                    <div className={'rating-block'}>
                        <h5 className={'rating-title'}>Quality of food: </h5>
                        <StarRatings
                            rating={this.state.quality}
                            changeRating={this.qualityRatingHandler}
                            starRatedColor="blue"
                            numberOfStars={6}
                            starDimension="15px"
                            starSpacing="2px"
                        />
                    </div>
                    <div className={'rating-block'}>
                        <h5 className={'rating-title'}>Service quality: </h5>
                        <StarRatings
                            rating={this.state.service}
                            changeRating={this.serviceRatingHandler}
                            starRatedColor="blue"
                            numberOfStars={6}
                            starDimension="15px"
                            starSpacing="2px"
                        />
                    </div>
                    <div className={'rating-block'}>
                        <h5 className={'rating-title'}>Interior: </h5>
                        <StarRatings
                            rating={this.state.interior}
                            changeRating={this.interiorRatingHandler}
                            starRatedColor="blue"
                            numberOfStars={6}
                            starDimension="15px"
                            starSpacing="2px"
                        />
                    </div>
                    </div>
                    <Button
                        type="submit" color="primary"
                        onClick={this.createReview}>
                        Send
                    </Button>

                    <div className={'upload-images'}>
                        <h2>Upload new image</h2>
                        <FormElement
                            propertyName="image"
                            title="Select image"
                            type="file"
                            required
                            onChange={this.fileChangeHandler}
                        />
                        <Button
                            type="submit" color="primary"
                            onClick={this.uploadImage}>
                            Upload
                        </Button>
                    </div>
                </div>
                }

            </div>
        );
    }

    qualityRatingHandler = value => {
        this.setState({quality: value})
    };

    serviceRatingHandler = value => {
        this.setState({service: value})
    };

    interiorRatingHandler = value => {
        this.setState({interior: value})
    }

}

const mapStateToProps = (state) => ({
    information: state.institutions.information,
    user: state.users.user,
    reviews: state.reviews.reviews,
    images: state.images.images
});

const mapDispatchToProps = dispatch => ({
    fetchFullInfo: id => dispatch(fetchFullInfo(id)),
    createReviewSuccess: (id, data) => dispatch(createReviewSuccess(id, data)),
    fetchReviews: id => dispatch(fetchReviews(id)),
    uploadsImage: (id, data) => dispatch(uploadsImage(id, data)),
    fetchImages: id => dispatch(fetchImages(id)),
    removeImage: (id, instId) => dispatch(removeImage(id, instId)),
    removeReview: (id, instId) => dispatch(removeReview(id, instId))
});

export default connect(mapStateToProps, mapDispatchToProps)(FullInformation);
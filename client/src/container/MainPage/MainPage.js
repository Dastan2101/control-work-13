import React, {Component} from 'react';
import {connect} from "react-redux";
import {apiURL} from "../../constants";
import {Card, CardBody, CardImg, CardSubtitle, CardTitle, Col, NavLink, Row} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import {fetchInstitutions, removeInstitutions} from "../../store/actions/institutionsActions";

class MainPage extends Component {

    componentDidMount() {
        this.props.fetchInstitutions()
    }

    render() {
        const {institutions, user} = this.props
        return (
            <Row>
                {
                    institutions && institutions.map(item => (
                        <React.Fragment key={item._id}>
                            <Col xs="6" sm="4">
                                <NavLink tag={RouterNavLink}
                                         to={`/information/${item._id}`}
                                         exact
                                         id={'dandelyan'}
                                >
                                <Card>
                                    <CardImg top width="100%" src={apiURL + '/uploads/' + item.image} alt="image"/>
                                    <CardBody>
                                        <CardTitle>
                                                {item.title}
                                        </CardTitle>
                                    </CardBody>
                                </Card>
                                </NavLink>
                                {user &&
                                user.role === 'admin' &&
                                <button className={'removeIcon'}
                                        onClick={() => this.props.removeInstitutions(item._id)}/>}
                            </Col>
                        </React.Fragment>
                    ))

                }
            </Row>
        );
    }
}

const mapStateToProps = (state) => ({
    institutions: state.institutions.institutions,
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    fetchInstitutions: () => dispatch(fetchInstitutions()),
    removeInstitutions: id => dispatch(removeInstitutions(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
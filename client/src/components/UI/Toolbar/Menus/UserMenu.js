import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavbarBrand, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const UserMenu = ({user, logout}) => (
    <React.Fragment>
        <NavbarBrand tag={RouterNavLink} to="/add">Add new place</NavbarBrand>

        <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
                Hello, {user.username}
            </DropdownToggle>
            <DropdownMenu right>
                <DropdownItem onClick={logout}>
                    Logout
                </DropdownItem>
            </DropdownMenu>
        </UncontrolledDropdown>
    </React.Fragment>
);

export default UserMenu;
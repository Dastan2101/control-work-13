import axios from "../../axios-api";

export const FETCH_IMAGES_SUCCESS = 'FETCH_IMAGES_SUCCESS';

const fetchImagesSuccess = data => ({type: FETCH_IMAGES_SUCCESS, data});

export const fetchImages = (id) => {
    return dispatch => {
        return axios.get('/images/' + id).then(
            response => dispatch(fetchImagesSuccess(response.data))
        )
    }
};

export const uploadsImage = (id, data) => {
    return dispatch => {
        return axios.post(`/images?institution=${id}`, data).then(
            () => dispatch(fetchImages(id))
        )
    }
};

export const removeImage = (id, instId) => {
    return dispatch => {
        return axios.delete(`/images/${id}`).then(
            () => dispatch(fetchImages(instId))
        )
    }
};
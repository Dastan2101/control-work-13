import axios from '../../axios-api';
import {push} from 'connected-react-router';

export const FETCH_INSTITUTIONS_SUCCESS = 'FETCH_INSTITUTIONS_SUCCESS ';

export const FETCH_INFORMATION_SUCCESS = 'FETCH_INFORMATION_SUCCESS'

const fetchInstitutionsSuccess = data => ({type: FETCH_INSTITUTIONS_SUCCESS, data});

const fetchInformationSuccess = data => ({type: FETCH_INFORMATION_SUCCESS, data});

export const fetchInstitutions = () => {
    return dispatch => {
        return axios.get('/institutions').then(
            response => dispatch(fetchInstitutionsSuccess(response.data))
        )
    }
};

export const fetchFullInfo = (id) => {
    return dispatch => {
        return axios.get('/institutions/' + id).then(
            response => dispatch(fetchInformationSuccess(response.data))
        )
    }
};

export const createPlace = (data) => {
    return dispatch => {
        return axios.post('/institutions', data).then(
            () => {
                dispatch(push('/'))
            }
        )
    }
};

export const removeInstitutions = (id) => {
    return dispatch => {
        return axios.delete(`/institutions/${id}`).then(
            () => dispatch(fetchInstitutions())
        )
    }
};
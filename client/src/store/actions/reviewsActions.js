import axios from '../../axios-api';

export const FETCH_REVIEWS_SUCCESS = 'FETCH_REVIEWS_SUCCESS';

const fetchReviewsSuccess = data => ({type: FETCH_REVIEWS_SUCCESS, data});

export const fetchReviews = id => {
    return dispatch => {
        return axios.get('/review/' + id).then(
            response => dispatch(fetchReviewsSuccess(response.data))
        )
    }
};

export const createReviewSuccess = (id, data) => {
    return dispatch => {
        return axios.post(`/review?institution=${id}`, data).then(
            () => dispatch(fetchReviews(id))
        )
    }
};

export const removeReview = (id, instId) => {
    return dispatch => {
        return axios.delete(`/review/${id}`).then(
            () => dispatch(fetchReviews(instId))
        )
    }
};
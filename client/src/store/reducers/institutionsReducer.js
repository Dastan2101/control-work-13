import {
    FETCH_INFORMATION_SUCCESS,
    FETCH_INSTITUTIONS_SUCCESS,
} from "../actions/institutionsActions";


const initialState = {
    institutions: [],
    information: null
};

const instReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_INSTITUTIONS_SUCCESS:
            return {...state, institutions: action.data};
        case FETCH_INFORMATION_SUCCESS:
            return {...state, information: action.data}
        default:
            return state;
    }
};

export default instReducer;

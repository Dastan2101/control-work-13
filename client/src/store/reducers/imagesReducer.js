import {FETCH_IMAGES_SUCCESS} from "../actions/imagesActions";


const initialState = {
    images: []
};

const imageReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_IMAGES_SUCCESS:
            return {...state, images: action.data};
        default:
            return state;
    }
};

export default imageReducer;

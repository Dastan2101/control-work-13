import {FETCH_REVIEWS_SUCCESS} from "../actions/reviewsActions";


const initialState = {
    reviews: []
};

const reviewReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_REVIEWS_SUCCESS:
            return {...state, reviews: action.data};
        default:
            return state;
    }
};

export default reviewReducer;
